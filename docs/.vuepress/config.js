module.exports = {
    lang: 'zh-CN',
    title: '前端指南',
    description: '',
    head: [['link', { rel: 'icon', href: '/favicon.ico' }]],
    themeConfig: { 
        logo: '/image/logo.png',
        sidebar:[
        // SidebarItem

        {
            text: '工程化',
            children: [
              // SidebarItem
              {
                text: 'Babel',
                link: '/engineering/Babel.md',
                children: [],
              },
            ],
          },
      ],},
  
}